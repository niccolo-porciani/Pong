#include <iostream>
#include <GL/glut.h>
#include "gl.h"

int main (int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(500,500);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Pong");
	
	glutDisplayFunc(display); // Chiamata ogni volta che bisogna ridisegnare la finestra.
	
	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
	glutKeyboardFunc(keyDown); // Key down
	glutKeyboardUpFunc(keyUp); // Key up
	
	resetGame();
	cout << "Comandi: Q/A, P/L\nPremi BARRA SPAZIATRICE per cominciare\n";
	timer(0);
	glutMainLoop();
	
	
	return 0;
}

